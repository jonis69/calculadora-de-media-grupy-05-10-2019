#############################################################################################
#  Código comentado da calculadora de média apresentada no GruPy-Mossoró do dia 05/10/2019  #
#                                                                                           #
#                Feito por: Gabriel Nascimento Jales e Jonas Silva Rodrigues                #
#############################################################################################

from tkinter import * #aqui importamos a biblioteca tkinter

def media(): #aqui será definida nossa função de cálculo de média
    try: #usando try/except, nós conseguiremos tratar os casos em que o usuário entre com uma letra ao invés de número
        v1 = float(valor1.get()) #criamos uma variável para receber o conteúdo que está na Entry "valor1" e convertemos para um valor float, pois o campo Entry trata os valores nele inseridos como string
        v2 = float(valor2.get()) #mesma coisa para o valor2
        lblResultado["text"]= "Resultado da média: {}".format((v1+v2)/2) #aqui mudaremos o campo ["text"] da futura Label "lblResultado" para o resultado do nosso cálculo
    except ValueError:
        lblResultado["text"]= "Entradas inválidas" #caso a entrada seja diferente de um número, então iremos retornar uma mensagem de erro na Label "lblResultado"

janela = Tk() #a classe Tk() define a janela principal
janela.geometry('400x400+0+0') #aqui definimos o tamanho da janela e a distância dela das bordas da tela

boasVindas = Frame(janela, width=400, height=100) #aqui criamos uma variável e atribuimos um Frame e dentro dos parametros colocamos onde será adicionado, e o tamanho em relação a janela
boasVindas.pack(side=TOP) #aqui usamos o gerenciador de layout pack para dizer a direção que o frame ficará posicionado

lblBemVindo = Label(boasVindas, font=('arial', 25, 'bold'), text="Calculadora de média", width=30) #aqui criamos outra variável e atribuimos um Label a ela, e dentro dos parametros colocamos onde será adicionado a fonte do texto (tamanho, tipo da fonte), o texto e a largura do label
lblBemVindo.place(relx=0.5, rely=0.5, anchor=CENTER) #aqui o gerenciador de layout place é usado para definirmos a posição x e y do label em relação ao tamanho da tela

valor1 = Entry(janela, width=20) #para podermos adicionar uma entrada de texto, devemos criar uma variável e atribuir a ela um Entry e definir onde ele será colocado e a largura dele
valor1.place(relx=0.5, rely=0.3, anchor=CENTER) #aqui usamos novamente o gerenciador de layout place para posicionar nosso Entry
valor2 = Entry(janela, width=20) #criamos outra variável para adicionarmos nosso segundo campo de texto
valor2.place(relx=0.5, rely=0.4, anchor=CENTER) #posicionando nosso campo de texto

media = Button(janela, width=10, text="Calcular ", command=media) #agora usaremos a variável média para criarmos um botão para executarmos nosso cálculo. Nele definimos onde será posicionado, o texto e qual comando ele irá executar, no caso uma função que ainda será definida por nós
media.place(relx=0.5, rely=0.6, anchor=CENTER) #usando novamente o gerenciador place para posicionar nosso botão
sair = Button(janela, width=20, text="Sair do programa", command=quit)#botão que possui a função de fechar a janela do programa, para isso ele recebe o comando "quit" do próprio tkinter
sair.place(relx=0.5, rely=0.9, anchor=CENTER) #posicionando nosso botão

#por fim, vamos criar três Labels diferentes, duas para colocarmos um texto ao lado de nossas entradas de valores e outra para indicar o resultado do nosso cálculo
lblValor1 = Label(janela, text="Valor 1") #label para nosso valor1
lblValor1.place(relx=0.2, rely=0.3, anchor=CENTER) #posicionando a label
lblValor2 = Label(janela, text="Valor 2") #label para nosso valor2
lblValor2.place(relx=0.2, rely=0.4, anchor=CENTER) #posicionando a label
lblResultado = Label(janela, text="Resultado da média: ") #label para mostrar o resultado do cálculo
lblResultado.place(relx=0.31, rely=0.5, anchor=CENTER) #posicionando a label

janela.title("Calculadora do GruPy") #aqui é definido o título da janela
janela.mainloop() #o mainloop é responsável por manter a janela rodando