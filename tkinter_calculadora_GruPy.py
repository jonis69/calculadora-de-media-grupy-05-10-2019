#################################################################################################
#  Código não comentado da calculadora de média apresentada no GruPy-Mossoró do dia 05/10/2019  #
#                                                                                               #
#                   Feito por: Gabriel Nascimento Jales e Jonas Silva Rodrigues                 #
#################################################################################################

from tkinter import *

def media():
    try:
        v1 = float(valor1.get())
        v2 = float(valor2.get())
        lblResultado["text"]= "Resultado da média: {}".format((v1+v2)/2)
    except ValueError:
        lblResultado["text"]= "Entradas inválidas"

janela = Tk()
janela.geometry('400x400+0+0')

boasVindas = Frame(janela, width=400, height=100)
boasVindas.pack(side=TOP)

lblBemVindo = Label(boasVindas, font=('arial', 25, 'bold'), text="Calculadora de média", width=30)
lblBemVindo.place(relx=0.5, rely=0.5, anchor=CENTER)

valor1 = Entry(janela, width=20)
valor1.place(relx=0.5, rely=0.3, anchor=CENTER)
valor2 = Entry(janela, width=20)
valor2.place(relx=0.5, rely=0.4, anchor=CENTER)

media = Button(janela, width=10, text="Calcular ", command=media)
media.place(relx=0.5, rely=0.6, anchor=CENTER)
sair = Button(janela, width=20, text="Sair do programa", command=quit)
sair.place(relx=0.5, rely=0.9, anchor=CENTER)

lblValor1 = Label(janela, text="Valor 1")
lblValor1.place(relx=0.2, rely=0.3, anchor=CENTER)
lblValor2 = Label(janela, text="Valor 2")
lblValor2.place(relx=0.2, rely=0.4, anchor=CENTER)
lblResultado = Label(janela, text="Resultado da média: ")
lblResultado.place(relx=0.31, rely=0.5, anchor=CENTER)

janela.title("Calculadora do GruPy")
janela.mainloop()